from ryu.base import app_manager
from ryu.controller import ofp_event
from ryu.controller.handler import CONFIG_DISPATCHER, MAIN_DISPATCHER
from ryu.controller.handler import set_ev_cls
from ryu.ofproto import ofproto_v1_3
from ryu.lib.packet import packet
from ryu.lib.packet import ethernet
from ryu.lib.packet import ether_types
from ryu.lib.packet import arp
from ryu.lib.packet import icmp
from ryu.lib.packet import ipv4
from lib_beta import rlib
from ryu.services.protocols.bgp.bgpspeaker import BGPSpeaker
from ryu.lib.packet import in_proto
from ryu.lib.packet import ipv4
from ryu.lib.packet import icmp
from ryu.lib.packet import tcp
from ryu.lib.packet import udp
from threading import Thread, Timer
from socketIO_client import SocketIO, BaseNamespace
import sys
import logging
from netaddr import *
import json
import time
import redis
from pyfiglet import Figlet

# Criando o banner do AS-Defender
banner = Figlet(font='slant')
print(banner.renderText('AS-Defender'))
print("USANDO SDN PARA COMBATER O SEQUESTRO DE PREFIXOS IP NA INTERNET")
print("AUTHORS: " + "MARCIO VINICIUS DE QUEIROZ SANTOS / SIDNEY LUCENA - UNIVERSIDADE FEDERAL DO ESTADO DO RIO DE JANEIRO")
time.sleep(5)

# Manipulador de logs
log = logging.getLogger()
log.setLevel(logging.INFO)
# Instancia as estruturas do switch
rlib = rlib()
# Define algumas informações hardcoded como o canal de comunicação entre a thread e o Ryu e as informações de vizinhos.
CHANNEL = "bgp_channel"

local_as_number = 65507
router_id = "192.168.1.1"

R1 = "192.168.1.2"
R2 = "192.168.2.2"
R3 = "192.168.3.2"
R4 = "192.168.4.2"
R5 = "192.168.5.2"

monitor_ip = "172.17.0.2"
port = 5000
prefix = '0.0.0.0/0' 
INTERVAL = 1


with open('config/as-defender.json') as configfile:
    customers = json.load(configfile)

prefixes_list = []

for customer in customers['customers_list']:
    prefixes_list.append(customer['prefix'])


class RepeatedTimer(object):
    """
    Define uma classe de timer para a realização de consulta no canal compartilhado

    """
    def __init__(self, interval, function, *args, **kwargs):
        self._timer = None
        self.interval = interval
        self.function = function
        self.args = args
        self.kwargs = kwargs
        self.is_running = False
        self.start()

    def _run(self):
        self.is_running = False
        self.start()
        self.function(*self.args, **self.kwargs)

    def start(self):
        if not self.is_running:
            self._timer = Timer(self.interval, self._run)
            self._timer.start()
            self.is_running = True

    def stop(self):
        self._timer.cancel()
        self.is_running = False


class ExaNamespace(BaseNamespace):
    """
    Classe para o cliente socketIO

    """

    def __init(self):
        self.r = redis.StrictRedis(host='localhost')


    def on_connect(self, *args):
        log.info('*connect %s', args)
        self.r = redis.StrictRedis(host='localhost')

    def on_disconnect(self, *args):
        log.info('*disconnect %s', args)

    def on_reconnect(self, *args):
        self.emit('exa_subscribe', {'prefix': prefix})
        log.info('*reconnect %s', args)

    def send_data(self, *data):
        self.r.publish(CHANNEL, json.dumps(data))
    # Método que captura o anúncio e implementa o algoritmo de detecção do ataque
    def on_exa_message(self, *args):
        log.info('*on_exa_message* %s', args) 

        for prefix in prefixes_list:
            if IPNetwork(args[0]['prefix']) in IPNetwork(prefix):
                customer_id = prefixes_list.index(prefix)

                if args[0]['path'][-1] != customers['customers_list'][customer_id]['asn']:
                    if args[0]['path'][-1] not in customers['customers_list'][customer_id]['moas']:
                        print("Prefix Hijacking Detected - Origin AS Path - Type 0")
                        print("Customer ASN Affected: " + str(customers['customers_list'][customer_id]["asn"]))
                        print("Customer ID: " + str(customer_id))
                        print("Hijacked Prefix " + str(args[0]['prefix']))
                        print("Attacker AS: " + str(args[0]['path'][-1]))
                        dados_mitigacao = {"prefixes": customers['customers_list'][customer_id]["subprefixes"], "nexthop": customers['customers_list'][customer_id]["nexthop"], "announce": customers['customers_list'][customer_id]["announce"]}
                        #print(dados_mitigacao)
                        self.send_data(dados_mitigacao)
                        
                        continue
                    elif len(args[0]['path']) > 1:
                            if args[0]['path'][-2] not in customers['customers_list'][customer_id]['moasneighbors']:
                                print("Prefix Hijacking Detected - Changing AS Path - Type 1")
                                print("Customer ASN Affected: " + str(customers['customers_list'][customer_id]["asn"]))
                                print("Customer ID: " + str(customer_id))
                                print("Hijacked Prefix " + str(args[0]['prefix']))
                                print("Attacker AS: " + str(args[0]['path'][-1]))
                                dados_mitigacao = {"prefixes": customers['customers_list'][customer_id]["subprefixes"], "nexthop": customers['customers_list'][customer_id]["nexthop"], "announce": customers['customers_list'][customer_id]["announce"]}
                                print(dados_mitigacao)
                                self.send_data(dados_mitigacao)
                                
                                continue
                        
                elif len(args[0]['path']) > 1:
                    if args[0]['path'][-2] not in customers['customers_list'][customer_id]['neighbors']:
                                print("Prefix Hijacking Detected - Changing AS Path - Type 1")
                                print("Customer ASN Affected: " + str(customers['customers_list'][customer_id]["asn"]))
                                print("Customer ID: " + str(customer_id))
                                print("Hijacked Prefix " + str(args[0]['prefix']))
                                print("Attacker AS: " + str(args[0]['path'][-1]))
                                dados_mitigacao = {"prefixes": customers['customers_list'][customer_id]["subprefixes"], "nexthop": customers['customers_list'][customer_id]["nexthop"], "announce": customers['customers_list'][customer_id]["announce"]}
                                print(dados_mitigacao)
                                self.send_data(dados_mitigacao)                                
                                
                                continue 
        

def bgp_monitor(host, port):
    '''
    host: Host ExaBGP para conectar
    port: Porta usada pelo ExaBGP Monitor
    prefix: Prefixo para Monitorar
    '''
    try:

        log.info('host: %s  port %s ', host, port)

        # Abrindo a conexao com o servidor sockerIO

        socketIO = SocketIO(host, port)

        # Subscrevendo para receber os eventos. É necessário passar o prefixo para ser monitorado.
        # Embora utilize o Ryu o namespace /onos é passado em função do código do colector de rotas ser de um outro projeto que usou o ONOS. Isso não foi modificado no coletor.
        exa_namespace = socketIO.define(ExaNamespace, '/onos')  
        exa_namespace.emit('exa_subscribe', {'prefix': prefix})

        #Esperando por novos eventos recebidos
        socketIO.wait()
    except KeyboardInterrupt:
        log.error("Canceled by user")

    except Exception as e:
        log.error("Unknown by user %s", e)



class BGPRouter13(app_manager.RyuApp):
    OFP_VERSIONS = [ofproto_v1_3.OFP_VERSION]

    def __init__(self, *args, **kwargs):
        super(BGPRouter13, self).__init__(*args, **kwargs)
        self.mac_to_port = {}
        self.ip_to_port = {}

        # Inicialização do cliente REDIS
        self.rc = redis.StrictRedis(host='localhost')
        self.rp = self.rc.pubsub(ignore_subscribe_messages=True)
        self.rp.subscribe(CHANNEL)

        self.bgp_speaker = BGPSpeaker(local_as_number, router_id,
                                      ssh_console=True, ssh_port=4999,
                                      ssh_host='localhost',
                                      best_path_change_handler=self.best_path_change_handler )
        self.bgp_speaker.neighbor_add(R1, 65502)
        self.bgp_speaker.neighbor_add(R2, 65504)
        self.bgp_speaker.neighbor_add(R3, 65505)
        self.bgp_speaker.neighbor_add(R4, 65503)
        self.bgp_speaker.neighbor_add(R5, 65506)

        # Inicialização da thread de monitoramento
        monitoring_Thread = Thread(target=bgp_monitor, args=(monitor_ip,port ))
        monitoring_Thread.daemon = True
        monitoring_Thread.start()

        # inicializando o Timer para receber as mensagens para a mitigação
        rt = RepeatedTimer(INTERVAL, self.check_messages)

    def check_messages(self):
        rdata = self.rp.get_message()
        if rdata != None:
            self.logger.info("received data %s", rdata)
            body = json.loads(str(rdata['data']))
            print(body)
            print("Performing Mitigation Actions...\n")
            time.sleep(120)
            if body[0]["announce"] == 1:
                print("Announce Mode Detected\n")    
                for prefix in body[0]["prefixes"]:
                    print("Announcing prefix " + prefix + " for mitigation\n")
                    self.bgp_speaker.prefix_add(prefix)
                    # Atualização das rotas para a mitigação
                    rlib.remove_route(network=prefix)
                    rlib.add_route(network=prefix, nexthop=body[0]["nexthop"])
            else:
                print("No Announce Mode Detected\n")
                print("Only adjusting Data Plane information...")
                for prefix in body[0]["prefixes"]:
                    rlib.remove_route(network=prefix)
                    rlib.add_route(network=prefix, nexthop=body[0]["nexthop"])


    def best_path_change_handler(self, ev):
        self.logger.info('best path changed:')
        self.logger.info('prefix: %s', ev.prefix)
        self.logger.info('nexthop: %s', ev.nexthop)
        self.logger.info('is_withdraw: %s', ev.is_withdraw)
        if ev.is_withdraw is True:
            rlib.remove_route(network=ev.prefix, nexthop=ev.nexthop)
        else:
            rlib.remove_route(network=ev.prefix)
            rlib.add_route(network=ev.prefix, nexthop=ev.nexthop)
        
    #Manipulacao evento Feature Reply recebido pelo Switch
    @set_ev_cls(ofp_event.EventOFPSwitchFeatures, CONFIG_DISPATCHER)
    def switch_features_handler(self, ev):
        datapath = ev.msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        match = parser.OFPMatch()
        actions = [parser.OFPActionOutput(ofproto.OFPP_CONTROLLER,
                                          ofproto.OFPCML_NO_BUFFER)]
        self.add_flow(datapath, 0, match, actions, idletimeout=0)

        # Adiciona fluxo ARP ao IP de gerenciamento
        for i in rlib.router_if:
            match = parser.OFPMatch(in_port=i["port"],eth_type=ether_types.ETH_TYPE_ARP)
            actions = [parser.OFPActionOutput(ofproto.OFPP_LOCAL)]
            self.add_flow(datapath, 1, match, actions, idletimeout=0)


        # Adiciona fluxo IP ao IP de gerenciamento(Isso inclui o ICMP, BGP etc.)
        for dstip in rlib.ips:
            match = parser.OFPMatch(eth_type=ether_types.ETH_TYPE_IP, ipv4_dst=dstip)
            actions = [parser.OFPActionOutput(ofproto.OFPP_LOCAL)]
            self.add_flow(datapath, 1, match, actions, idletimeout=0)

    def add_flow(self, datapath, priority, match, actions, buffer_id=None, idletimeout=0):
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser

        inst = [parser.OFPInstructionActions(ofproto.OFPIT_APPLY_ACTIONS,
                                             actions)]
        if buffer_id:
            mod = parser.OFPFlowMod(datapath=datapath, buffer_id=buffer_id,
                                    idle_timeout=idletimeout, priority=priority, match=match,
                                    instructions=inst)
        else:
            mod = parser.OFPFlowMod(datapath=datapath, idle_timeout=idletimeout, priority=priority,
                                    match=match, instructions=inst)
        datapath.send_msg(mod)


    @set_ev_cls(ofp_event.EventOFPPacketIn, MAIN_DISPATCHER)
    def _packet_in_handler(self, ev):
        # If you hit this you might want to increase
        # the "miss_send_length" of your switch
        if ev.msg.msg_len < ev.msg.total_len:
            self.logger.debug("packet truncated: only %s of %s bytes",
                              ev.msg.msg_len, ev.msg.total_len)
        msg = ev.msg
        datapath = msg.datapath
        ofproto = datapath.ofproto
        parser = datapath.ofproto_parser
        in_port = msg.match['in_port']

        pkt = packet.Packet(msg.data)
        eth = pkt.get_protocols(ethernet.ethernet)[0]

        if eth.ethertype == ether_types.ETH_TYPE_LLDP:
            # ignore lldp packet
            return
        dst = eth.dst
        src = eth.src
        srcip = None
        dstip = None
        dpid = datapath.id
        self.mac_to_port.setdefault(dpid, {})
        self.ip_to_port.setdefault(dpid, {})


        self.logger.info("packet in %s %s %s %s", dpid, src, dst, in_port)

        # Aprende o MAC para evitar Floodar na proxima vez
        self.mac_to_port[dpid][src] = in_port

        # Checa se é um pacote ARP (realiza a operação FLOODING)
        if eth.ethertype == ether_types.ETH_TYPE_ARP:
            self.logger.info("Received ARP Packet %s %s %s ", dpid, src, dst)
            a = pkt.get_protocol(arp.arp)
            data = None
            if msg.buffer_id == ofproto.OFP_NO_BUFFER:
                data = msg.data
            actions = [parser.OFPActionOutput(ofproto.OFPP_FLOOD)]
            out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                      in_port=in_port, actions=actions, data=data)
            datapath.send_msg(out)
            return


        if eth.ethertype == ether_types.ETH_TYPE_IP:
            ip = pkt.get_protocol(ipv4.ipv4)
            srcip = ip.src
            dstip = ip.dst
            self.ip_to_port[dpid][srcip] = in_port
            self.logger.info("Received IP Packet %s %s %s ", dpid, srcip, dstip)
            # Encaminhamento de IP
            route = rlib.lookup_routing_table(dstip)
            print("route :", route)
            if route:
                outport = route["port"]
                ethsrc = rlib.get_port_data(outport)["mac"]
                rlib.print_neighbor()
                if route["nexthop"]:
                    ethdst = rlib.get_neighbor(route["nexthop"])
                    self.logger.info("Routing details: outport %s ethsrc %s ethdst %s ",
                                     str(outport), ethsrc, ethdst)
                    if ethdst is None:
                        sip = rlib.get_port_data(outport)["ip"]                           
                        self.send_arp_request(datapath, ethsrc, sip, route["nexthop"], outport)
                        return
                elif route["scope"] == "link":
                    ethdst = rlib.get_neighbor(dstip)
                    self.logger.info("Routing details: outport %s ethsrc %s ethdst %s ",
                                     str(outport), ethsrc, ethdst)                        
                    if ethdst is None:
                        sip = rlib.get_port_data(outport)["ip"]
                        self.send_arp_request(datapath, ethsrc, sip, dstip, outport)
                        return

                actions = []
                match = parser.OFPMatch(in_port=in_port, eth_type=ether_types.ETH_TYPE_IP,
                                        ipv4_src=srcip,
                                        ipv4_dst=dstip
                                        )
                # Procedimento de reescrita de MAC
                actions.append(parser.OFPActionSetField(eth_src=ethsrc))
                actions.append(parser.OFPActionSetField(eth_dst=ethdst))
                actions.append(parser.OFPActionOutput(outport))

                if msg.buffer_id != ofproto.OFP_NO_BUFFER:
                    self.add_flow(datapath, 1, match, actions, msg.buffer_id, idletimeout=10)
                    return
                else:
                    self.add_flow(datapath, 1, match, actions, idletimeout=10)

                data = None
                if msg.buffer_id == ofproto.OFP_NO_BUFFER:
                    data = msg.data

                out = parser.OFPPacketOut(datapath=datapath, buffer_id=msg.buffer_id,
                                          in_port=in_port, actions=actions, data=data)
                datapath.send_msg(out)
