import ipaddress
import yaml
import sys
"""
Metodo para validar se um IP é correspondente a uma rede. É usado para validar se a tabela de rotas possui um caminho para um determinado IP.
"""
def network_match(network, ip):
    result = ipaddress.ip_address(unicode(ip)) in ipaddress.ip_network(unicode(network))
    #result = ipaddress.ip_address(ip) in ipaddress.ip_network(network)
    return result

class rlib(object):
    """
    Classe para gerenciamento do switch OpenFlow. Aqui são instanciadas as algumas estruturas de dados referentes ao plano de controle do switch como a tabela de rotas, tabela ARP, interfaces e os IP's.
    """

    def __init__(self):
        self.routes = []
        self.arp_tables = []
        self.router_if = []
        self.ips = []

        self.tdata = {}
        try:
            with open("input.yaml") as fp:
                self.tdata = yaml.load(fp)
        except Exception as e:
            #print "Error reading input file ", e.__doc__
            #print e.message
            print("Error reading input file ", e.__doc__)
            print(e.message)         
            sys.exit(1)

        self.router_if = self.tdata['router']['interfaces']
        for i in self.router_if:
            self.ips.append(i["ip"])

        # Populando as rotas a partir da tabela de interfaces e das rotas estáticas
        self.populate_routes()
        print("Started Router with interface ", self.router_if)
        print("Routing Table ", self.routes)

        # Adicionando os vizinhos através do MAC e IP hardcoded. Dados dos vizinhos foram definidos nos conteineres Docker
        self.add_neighbor("00:00:00:00:00:11", "192.168.1.2")
        self.add_neighbor("00:00:00:00:00:15", "192.168.2.2")
        self.add_neighbor("00:00:00:00:00:16", "192.168.3.2")
        self.add_neighbor("00:00:00:00:00:18", "192.168.4.2")
        self.add_neighbor("00:00:00:00:00:20", "192.168.5.2")


    # Método para adicionar os detalhes dos vizinhos
    def add_neighbor(self, mac, ip):
        """
        mac: MAC address do vizinho
        ip: IP do vizinho

        """
        self.arp_tables.append({"mac": mac, "ip": ip})

    def get_neighbor(self, ip):
        """
        Obtém o MAC do vizinho com base no IP.

        IP: IP do vizinho
        """

        for arp_entry in self.arp_tables:
            if arp_entry["ip"] == ip:
                return arp_entry["mac"]
        return None

    def print_neighbor(self):
        print("neighbor tables ", self.arp_tables)

    def populate_routes(self):
        """
        Popula as rotas na tabela de rotas do switch. 

        """
        # Interfaces conectadas
        for i in self.router_if:
            a = ipaddress.ip_network(unicode(i["ip"] + '/' + i["mask"]),
                                     strict=False)
            #a = ipaddress.ip_network(i["ip"] + '/' + i["mask"],
            #                         strict=False)           
            network = str(a.network_address) + "/" + str(a.prefixlen)
            self.routes.append({"network": network, "port": i["port"],
                                "scope": "link", "nexthop": None})

        # Rotas estáticas
        if "routes" in self.tdata["router"]:
            for route in self.tdata["router"]["routes"]:
                # Precisa encontrar o número da porta do próximo salto

                self.routes.append({"network": route["subnet"], 
                                    "port": self.get_port_no_for_ip(route["nexthop"]),
                                    "scope": "static", "nexthop": route["nexthop"]})

    def lookup_routing_table(self, destip):
        '''
        Olha a tabela de roteamento para saber se existe uma rota para o prefixo passado. Caso haja, a rota e retornada. Caso contrário, é retornado "None"
        destip: IP de destino que se deseja alcançar.

        '''
        possible_routes = []
        prefixes = []

        for route in self.routes:
            if network_match(route["network"], destip):
                possible_routes.append(route)
        #print("possible_routes : ", possible_routes) 
        if len(possible_routes) == 1:
            return possible_routes[0]
        elif len(possible_routes) > 1:
            for route in possible_routes:
                network = ipaddress.ip_network(unicode(route["network"]))
                prefixes.append(network.prefixlen)

            return possible_routes[prefixes.index(max(prefixes))]             


        return None

    def add_route(self, network, nexthop=None, port=None, scope="bgp"):
        # Checa se a rota já existe. Caso positivo, não adiciona.
        print("add_route called with ", network, nexthop, port, scope)
        for route in self.routes:
            if route["network"] == network:
                print("route already exists", route)
                return
        if nexthop:
            self.routes.append({"network": network,
                                "port": self.get_port_no_for_ip(nexthop),
                                 "scope": scope, "nexthop": nexthop})

        elif port:
            self.routes.append({"network": network, "port": port,
                                "scope": scope, "nexthop": None})

        print("route added ", self.routes)
        return


    def remove_route(self, network, nexthop=None, scope="bgp"):
        '''
        Remove uma rota da tabela de roteamento do switch.
        network: A rede para onde a rota deve ser passada.
        nexthop: Padrão setado como None.
        scope: Indica o escopo da rota. Pode ter um escopo de "link" que significa as rotas diretamente conectadas. O escopo "bgp" indicam rotas que foram anunciadas e não estão diretamente conectadas.

        '''
#        newroutes = []
        for route in self.routes:
            if route["network"] == network and route["scope"] == scope:
               self.routes.pop(self.routes.index(route))
               print("Route Found - Removed", route)
               return
        
#        print("No route to remove")
        return
#                print("route already exists - removing", route)
#            else:
#                newroutes.append(route)
#        self.routes = newroutes
#        return


    def get_port_data(self, portno):

        '''
        Lê a lista de interfaces do roteador e retorna a entrada correspondente à porta passada.

        '''
        for i in self.router_if:
            if i["port"] == portno:
                return i
        return None


    def get_mac_for_ip(self, ip):
        '''
        Lê as interfaces do switch e retorna o endereço MAC correspondente ao IP passado.

        '''
        for i in self.router_if:
            if i["ip"] == ip:
                return i["mac"]
        return None

    def get_port_no_for_ip(self, ip):
        ''' 
        Função utilizada para popular a tabela de rotas estáticas.
        IP: O IP para onde se deseja saber para qual porta encaminhar o fluxo.

        '''
#        for i in self.router_if:
#            a = ipaddress.ip_network(unicode(i["ip"] + '/' + i["mask"]), strict=False)
#            network = str(a.network_address) + "/" + str(a.prefixlen)

        for i in self.routes:
            if network_match(i["network"], ip):
                return i["port"]
        return None
