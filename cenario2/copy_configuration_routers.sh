#/bin/bash!

cd /home/marciovqsantos/bgp_as_defender/v4_final/cenario2

sudo docker cp R1.conf R1:/etc/.
sudo docker cp R2.conf R2:/etc/.
sudo docker cp R3.conf R3:/etc/.
sudo docker cp R4.conf R4:/etc/.
sudo docker cp R5.conf R5:/etc/.
sudo docker cp R6.conf R6:/etc/.
sudo docker cp R7.conf R7:/etc/.
sudo docker cp R8.conf R8:/etc/.
sudo docker cp R9.conf R9:/etc/.
sudo docker cp R10.conf R10:/etc/.
sudo docker cp exabgp.conf ExaBGP:/etc/.
