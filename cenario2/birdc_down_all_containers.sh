#/bin/bash!



for container in `docker ps -q`; do 
  # show the name of the container
  docker inspect --format='{{.Name}}' $container;
  # run the command (date in the case)
  docker exec -it $container sh -c 'birdc down' $1;
done
