#/bin/bash!

cd /home/marciovqsantos/bgp_as_defender/v4_final/cenario1/

sudo docker cp R1.conf R1:/etc/.
sudo docker cp R2.conf R2:/etc/.
sudo docker cp R3.conf R3:/etc/.
sudo docker cp R4.conf R4:/etc/.
sudo docker cp R5.conf R5:/etc/.
sudo docker cp R6.conf R6:/etc/.
sudo docker cp exabgp.conf ExaBGP:/etc/.
