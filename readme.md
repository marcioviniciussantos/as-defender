AS-Defender - An autonomous system SDN-based to combat prefix hijacking in Internet - Marcio Vinicius de Queiroz Santos - Federal University Of State Of Rio de Janeiro
===================================================================================

AS-Defender is an autonomous system projected to perform the protection os prefix hijacking attack directed to it neighbor prefixes. When an attack is detected, AS-Defender perform some mitigation steps to induce the autonomous systems in the topology to prefer the route directed to the victim.

To use the AS-Defender is necessary to install the following components:

.Linux Ubuntu 16.04 LTS

.Knet Topology Builder - https://github.com/knetsolutions/KNet

.Python 2.7

.Ryu controller 4.36 - ryu python module

.Python Modules redis, pyfiglet, netaddr, python-socketio, yaml and ipaddress. Other built-in modules are used.

.Docker

All modules can be installed using python pip command

The Knet needs to be installed from Git Hub repository

After all installations perform the follow steps to reproduce an example experiment to visualize the AS-Defender behavior.

Step 1: Start the one of the scenarios(choose one of them)
=================================================
knet-cli

cd as-defender/cenarioX where X is the number of choosed scenario.

CreateTopology topology.yaml

Step 2: Verify if the topology is up
=================================================
sudo docker ps -a
sudo ovs-vsctl show
sudo ifconfig SDNRTR1           
sudo ip addr

Note the MAC address of SDNRTR1 and feed in to input.yaml file

Step 3: Edit the input 
=================================================
Perform the command "sudo ip addr" and find where is the SDNRTR1 MAC Address information. Copy it and paste the same value in all MAC fields on input.yaml file.

Step 4: Copy the BGP Config files to the Routers
=================================================
Execute the script copy_configuration_routers.sh

Step 5: Start the AS-Defender prototype
=================================================
cd src/

ryu-manager as_defender.py

Step 6: Start the bird routing application
=================================================
Open a new terminal for all nodes. Each on a separate tab.

1)

sudo docker exec -it R1 sh
bird -c /etc/R1.conf
birdc show protocols

Repeat the same for all other bird routers. In scenario1 the routers are named from R1 to R6 and in scenario2 these names are from R1 to R10.

other commands:

ip route
birdc show protocols all
tail -f /var/log/bird.log

Step 7: Start the WebSocket Route Collector Server
=================================================
sudo docker exec -it ExaBGP sh

chmod 777 server.py

env exabgp.log.all=true exabgp.log.level=DEBUG exabgp.log.destination=/var/log/exabgp.log exabgp.log.routes=true exabgp.daemon.user=root exabgp /etc/exabgp.conf &

How to reproduce the BGP attack
===============================
Now advertise the AS65503 (R4) prefix in AS65501 (R6) as below. To understand the experiment, see the topologia.png file to understand the arrangement of elements.

1) sudo docker exec -it R6 sh

2) In R6.conf, uncomment the static route code which advertises R4 prefix

3) reload the bird config using below command

4) birdc configure soft

5) Open the AS-Defender tab. You will see the following message:

"Prefix Hijacking Detected - Origin AS Path - Type 0
Customer ASN Affected: 65503
Customer ID: 0
Hijacked Prefix 10.3.1.0/24
Attacker AS: 65501
received data {'pattern': None, 'type': 'message', 'channel': 'bgp_channel', 'data': '[{"prefixes": ["10.3.1.0/24"], "nexthop": "192.168.4.2", "announce": 1}]'}
[{u'prefixes': [u'10.3.1.0/24'], u'nexthop': u'192.168.4.2', u'lightweight': 0}]
Performing Mitigation Actions..."

6) Try to ping and traceroute to 10.3.1.2 (Host inside AS65503) from R2 (AS65504). You won't be able to contact the host.

7) After a minute you will see the following message:

"Announcing prefix 10.3.1.0/24 for mitigation

API method network.add called with args: {'prefix': u'10.3.1.0/24'}
('Route Found - Removed', {'scope': 'bgp', 'nexthop': '192.168.1.2', 'network': '10.3.1.0/24', 'port': 1})
('add_route called with ', u'10.3.1.0/24', u'192.168.4.2', None, 'bgp')
('route added ', [{'scope': 'link', 'nexthop': None, 'network': '192.168.1.0/24', 'port': 1}, {'scope': 'link', 'nexthop': None, 'network': '192.168.2.0/24', 'port': 2}, {'scope': 'link', 'nexthop': None, 'network': '192.168.3.0/24', 'port': 3}, {'scope': 'link', 'nexthop': None, 'network': '192.168.4.0/24', 'port': 4}, {'scope': 'link', 'nexthop': None, 'network': '192.168.5.0/24', 'port': 5}, {'scope': 'bgp', 'nexthop': '192.168.1.2', 'network': '192.168.7.0/24', 'port': 1}, {'scope': 'bgp', 'nexthop': '192.168.1.2', 'network': '192.168.8.0/24', 'port': 1}, {'scope': 'bgp', 'nexthop': '192.168.2.2', 'network': '10.2.1.0/24', 'port': 2}, {'scope': 'bgp', 'nexthop': '192.168.4.2', 'network': '10.3.0.0/23', 'port': 4}, {'scope': 'bgp', 'nexthop': '192.168.1.2', 'network': '10.1.1.0/24', 'port': 1}, {'scope': 'bgp', 'nexthop': u'192.168.4.2', 'network': u'10.3.1.0/24', 'port': 4}])
*on_exa_message* ({u'timestamp': 1537888345, u'host': u'exabgp', u'prefix': u'10.3.1.0/24', u'peer': u'192.168.8.1', u'path': [65507], u'type': u'A'},)"

8) Try ping and traceroute again. You will be able to contact 10.3.1.2. The access to the victim prefix was mitigated successfully.
